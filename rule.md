# The GitLab Flow is based on 11 rules:

1. Use feature branches, no direct commits on master
2. Test all commits, not only ones on master
3. Run all the tests on all commits (if your tests run longer than 5 minutes have them run in parallel).
4. Perform code reviews before merges into master, not afterwards.
5. Deployments are automatic, based on branches or tags.
6. Tags are set by the user, not by CI.
7. Releases are based on tags.
8. Pushed commits are never rebased.
9. Everyone starts from master, and targets master.
10. Fix bugs in master first and release branches second.
11. Commit messages reflect intent.

1. feature branche를 사용하고 master에서 직접 커밋하지 않음
2. master의 커밋뿐만 아니라 모든 커밋을 테스트합니다.
3. 모든 커밋에 대해 모든 테스트를 실행합니다.
   (테스트가 5분 이상 실행되는 경우 병렬로 실행).
4. 이후가 아니라 마스터에 병합하기 전에 코드 검토를 수행합니다.
5. 배포는 분기 또는 태그를 기반으로 자동으로 이루어집니다.
6. 태그는 CI가 아닌 사용자가 설정합니다.
7. 릴리스는 태그를 기반으로 합니다.
8. 푸시된 커밋은 리베이스되지 않습니다.
9. 모든 사람은 마스터에서 시작하여 마스터를 대상으로 합니다.
10. 먼저 마스터의 버그를 수정하고 두 번째로 릴리스 분기를 수정합니다.
11. 커밋 메시지에는 의도가 반영됩니다.

프로젝트 별로 team을 구성하고, gitlab의 sub group을 활용한다. Team은 10명 미만으로 구성하기를 권장한다. 팀원들의 역활은 5역활로 구성된다. 각 역활별로 1명 이상의 지정이 필요하며, 1명이 여러 역활을 담당할 수 있다. 팀내에서 PM, 개발팀장은 유일해야 한다.

역활 배정별 권한 설정 가이드.
PM : 오너
개발팀장 : 메니저너?

## 프로젝트 git 규칙

git hook은 클라이언트의 보조도구, 서버는 gitlab ci/cd 활용.

- [x] main, pre-production, production branch에 직접 push하지 않는다.
- [x] hotfix/xxx, feature/xxx branch로만 만든다.
- [ ] main으로 MR하는 것은 main이 base 여야 한다. pre-production에서 main으로 MR할 수 없다.
- [ ] MR시 커밋메시지 규칙을 검사한다.
- [ ] MR시 codelint를 검사한다. 
- [ ] .gaitlab-ci.yml 파일을 수정해서는 안된다.
- [ ] pre-production으로 MR하는 것은 main이 base 여야 한다.
- [ ] production으로 MR하는 것은 production이 base 여야 하고, pipe에 성공해야한다.
